#include "AggressiveAIPlayerStrategy.h"

#include <iostream>

#include "io_utilities.h"
#include "Game.h"

AggressiveAIPlayerStrategy::AggressiveAIPlayerStrategy(Player* player) : AIPlayerStrategy(player)
{
}

AggressiveAIPlayerStrategy::~AggressiveAIPlayerStrategy()
{
}

void AggressiveAIPlayerStrategy::rollDice(std::vector<int>& results)
{
	int numDice = results.size();

	bool again = true;
	int numberOfRolls = 1;
	while (again && numberOfRolls < player->getMaxRolls())
	{
		std::vector<int> chosen;
		for (auto i = 0; i < results.size(); i++)
		{
			if (results[i] != static_cast<int>(Dice::EDiceSymbols::ATTACK) && results[i] != static_cast<int>(Dice::EDiceSymbols::DESTROY))
				chosen.push_back(i);
		}

		again = !chosen.empty();
		if (again)
		{
			io_utilities::typewrite(player->getName() + ", chooses to reroll some dice. \n");

			for (int i : chosen)
			{
				if (i >= 0 && i < results.size())
					results[i] = -1; // mark chosen index for removal
			}
			results.erase(remove(results.begin(), results.end(), -1), results.end()); // remove all marked index
			std::cout << "Locked dice: " << player->getDiceRoller()->resultsToString(results) << std::endl;
			std::vector<int> results2 = player->getDiceRoller()->roll(numDice - results.size());
			results.insert(results.end(), results2.begin(), results2.end());
			player->getDiceRoller()->setPreviousRoll(results);
		}

		numberOfRolls++;
	}
}

void AggressiveAIPlayerStrategy::resolveDice(std::map<Dice::EDiceSymbols, int> diceResults)
{
	if (!player->isDead())
	{
		if (!diceResults.empty())
		{
			if (diceResults.find(Dice::EDiceSymbols::ATTACK) != diceResults.end())
			{
				player->resolve(Dice::EDiceSymbols::ATTACK, diceResults[Dice::EDiceSymbols::ATTACK]);
				diceResults[Dice::EDiceSymbols::ATTACK] = -1; // mark as processed
			}
			if (diceResults.find(Dice::EDiceSymbols::DESTROY) != diceResults.end())
			{
				player->resolve(Dice::EDiceSymbols::DESTROY, diceResults[Dice::EDiceSymbols::DESTROY]);
				diceResults[Dice::EDiceSymbols::DESTROY] = -1; // mark as processed
			}

			for (auto it = diceResults.begin(); it != diceResults.end(); it++)
			{
				if (it->second != -1)
					player->resolve(it->first, it->second);
			}
		}
	}
}

bool AggressiveAIPlayerStrategy::flee()
{
	return AIPlayerStrategy::flee();
}

int AggressiveAIPlayerStrategy::destroy(std::vector<Deck<Tile>*> buildingTargets, std::vector<std::shared_ptr<Tile>> militaryTargets)
{
	return AIPlayerStrategy::destroy(buildingTargets, militaryTargets);
}

void AggressiveAIPlayerStrategy::move(std::vector<Borough*> movementOptions, bool mandatoryMovement)
{
	AIPlayerStrategy::move(movementOptions, mandatoryMovement);
}

void AggressiveAIPlayerStrategy::buyCards(std::vector<std::shared_ptr<Card>>& shop)
{
	AIPlayerStrategy::buyCards(shop);
}