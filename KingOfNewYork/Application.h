#pragma once

#include "Game.h"

class Application
{
public:
	Application();
	~Application();

	void start();
	void exit();
};

