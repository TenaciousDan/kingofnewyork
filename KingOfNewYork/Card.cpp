#include "Card.h"

#include <iostream>
#include "CardEffects.h"

using std::cout;

Card::Card(int id)
{
	this->id = id;
	cost = 0;
	type = CARD_TYPE::GOAL;
}

Card::Card(string name, int cost, CARD_TYPE type, string desc)
{
	this->name = name;
	this->cost = cost;
	this->type = type;
	this->desc = desc;
}

Card::~Card()
{
}

int Card::getId()
{
	return this->id;
}

string Card::getName()
{
	return this->name;
}

void Card::setName(string name)
{
	this->name = name;
}

int Card::getCost()
{
	return this->cost;
}

void Card::setCost(int cost)
{
	this->cost = cost;
}

Card::CARD_TYPE Card::getType()
{
	return this->type;
}

void Card::setType(CARD_TYPE type)
{
	this->type = type;
}

string Card::getDesc()
{
	return  this->desc;
}

void Card::setDesc(string desc)
{
	this->desc = desc;
}

void Card::activateEffect(Player* owner)
{
	try
	{
		if (CardEffects::conditions.at(id)(owner))
		{
			this->notify();
			CardEffects::effects.at(id)(owner);
		}
	}
	catch (std::out_of_range e)
	{
		std::cerr << "Card effect for card '" << name << "' has not been implemented yet.";
	}
}

void Card::render() const
{
	cout << name << '\n';
	cout << "--------------------" << '\n';
	cout << "(" << Card::cardTypeToString(type) << " | " << cost << ")" << "\n";
	cout << desc << '\n';
}

string Card::toString()
{
	string str = "";
	str += "{ name:" + this->name + ", cost:" + std::to_string(this->cost) + ", type:" + Card::cardTypeToString(this->type) + ", desc:" + this->desc + "}";
	return str;
}

string Card::cardTypeToString(CARD_TYPE type)
{
	if (type == CARD_TYPE::KEEP)
		return "KEEP";
	else if (type == CARD_TYPE::DISCARD)
		return "DISCARD";
	else
		return "GOAL";
}
