#pragma once

#include <map>
#include "Player.h"

class CardEffects
{
private:
	CardEffects(); // do not instantiate

public:
	~CardEffects();

	static const std::map<int, bool (*)(Player * owner)> conditions;
	static const std::map<int, void (*)(Player * owner)> effects;
};


