#include "CardObserver.h"

#include <iostream>

#include "Card.h"

using std::cout;

void CardObserver::onNotify(const Observable* observable)
{
	if (const Card* card = dynamic_cast<const Card*>(observable))
	{
		cout << "------------------------------------------------------------" << '\n';
		cout << "Card effect activated:" << '\n';
		card->render();
		cout << "------------------------------------------------------------" << '\n';
	}
}