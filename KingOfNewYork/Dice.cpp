#include "Dice.h"

#include <sstream>
#include <algorithm>

Dice::Dice() : Dice(6) {}
Dice::Dice(int numSides) : Dice(numSides, 1) {}
Dice::Dice(int numSides, int startAt) : values(numSides)
{
	for (int i = 0; i < numSides; i++)
		values[i] = startAt + i;
}

Dice::Dice(vector<int> values)
{
	this->values = values;
}

Dice::~Dice()
{
}

vector<int> Dice::getValues()
{
	return values;
}

void Dice::setValues(vector<int> values)
{
	this->values = values;
}

string Dice::diceSymbolToString(EDiceSymbols symbol)
{
	if (symbol == EDiceSymbols::ATTACK)
		return "ATTACK";
	else if (symbol == EDiceSymbols::DESTROY)
		return "DESTROY";
	else if (symbol == EDiceSymbols::ENERGY)
		return "ENERGY";
	else if (symbol == EDiceSymbols::HEART)
		return "HEART";
	else if (symbol == EDiceSymbols::OUCH)
		return "OUCH";
	else if (symbol == EDiceSymbols::VICTORY)
		return "VICTORY";
	else
		return "NONE";
}

Dice::EDiceSymbols Dice::strToDiceSymbol(string str)
{
	std::transform(str.begin(), str.end(), str.begin(), ::toupper);
	if (str == Dice::diceSymbolToString(EDiceSymbols::ATTACK))
		return EDiceSymbols::ATTACK;
	else if (str == Dice::diceSymbolToString(EDiceSymbols::DESTROY))
		return EDiceSymbols::DESTROY;
	else if (str == Dice::diceSymbolToString(EDiceSymbols::ENERGY))
		return EDiceSymbols::ENERGY;
	else if (str == Dice::diceSymbolToString(EDiceSymbols::HEART))
		return EDiceSymbols::HEART;
	else if (str == Dice::diceSymbolToString(EDiceSymbols::OUCH))
		return EDiceSymbols::OUCH;
	else if (str == Dice::diceSymbolToString(EDiceSymbols::VICTORY))
		return EDiceSymbols::VICTORY;
	else
		return EDiceSymbols::NONE;
}

string Dice::toString()
{
	std::ostringstream os;
	os << "[";
	for (auto i = 0; i < values.size(); i++)
	{
		os << values[i];
		if (i != (values.size() - 1)) os << ", ";
	}
	os << "]";

	return "values: " + string(os.str());
}

int Dice::roll()
{
	return values[(std::rand() % values.size())];
}

void Dice::render() const
{
	//
}

