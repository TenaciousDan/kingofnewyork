#pragma once

#include <string>
#include <vector>

#include "GameObject.h"

using std::string;
using std::vector;

class Dice : public GameObject
{
public:
	enum class EDiceSymbols { NONE, VICTORY, HEART, ENERGY, DESTROY, OUCH, ATTACK };

private:
	vector<int> values;

public:
	Dice();
	Dice(int numSides);
	Dice(int numSides, int startAt);
	Dice(std::vector<int> values);
	virtual ~Dice();

	std::vector<int> getValues();
	void setValues(vector<int> values);

	static std::string diceSymbolToString(EDiceSymbols symbol);
	static EDiceSymbols strToDiceSymbol(string str);

	virtual string toString();

	int roll();

	void render() const override;
};

