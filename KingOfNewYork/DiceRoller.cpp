#include "DiceRoller.h"

#include <iostream>
#include <iomanip>

#include "DiceObserver.h"

using std::cout;

DiceRoller::DiceRoller()
{
	totalRolls = 0;
}

DiceRoller::~DiceRoller()
{
}

void DiceRoller::render() const
{
	if (totalRolls > 0)
	{
		cout << "Dice rolling stats: ";
		for (auto it = resultStats.begin(); it != resultStats.end(); it++)
		{
			if (it != resultStats.begin())
				cout << ", ";

			float percentage = (float)it->second / (float)totalRolls * (float)100;

			if (it->first == static_cast<int>(Dice::EDiceSymbols::VICTORY))
				cout << Dice::diceSymbolToString(Dice::EDiceSymbols::VICTORY) << ": " << std::setprecision(2) << percentage << "%";
			else if (it->first == static_cast<int>(Dice::EDiceSymbols::HEART))
				cout << Dice::diceSymbolToString(Dice::EDiceSymbols::HEART) << ": " << std::setprecision(2) << percentage << "%";
			else if (it->first == static_cast<int>(Dice::EDiceSymbols::ENERGY))
				cout << Dice::diceSymbolToString(Dice::EDiceSymbols::ENERGY) << ": " << std::setprecision(2) << percentage << "%";
			else if (it->first == static_cast<int>(Dice::EDiceSymbols::DESTROY))
				cout << Dice::diceSymbolToString(Dice::EDiceSymbols::DESTROY) << ": " << std::setprecision(2) << percentage << "%";
			else if (it->first == static_cast<int>(Dice::EDiceSymbols::OUCH))
				cout << Dice::diceSymbolToString(Dice::EDiceSymbols::OUCH) << ": " << std::setprecision(2) << percentage << "%";
			else if (it->first == static_cast<int>(Dice::EDiceSymbols::ATTACK))
				cout << Dice::diceSymbolToString(Dice::EDiceSymbols::ATTACK) << ": " << std::setprecision(2) << percentage << "%";

		}
		cout << '\n';
	}
}

vector<int> DiceRoller::roll(int numberOfDice)
{
	vector<int> results;
	for (auto i = 0; i < numberOfDice; i++)
	{
		int result = die.roll();
		resultStats[result]++;
		results.push_back(result);
		totalRolls++;
	}
	previousRoll = results;

	this->notify();

	return results;
}

string DiceRoller::resultsToString(vector<int> results)
{
	vector<Dice::EDiceSymbols> resultSymbols;
	for (int i : results)
		resultSymbols.push_back(static_cast<Dice::EDiceSymbols>(i));

	string resultString = "[";
	for (unsigned int i = 0; i < resultSymbols.size(); i++)
	{
		resultString += (i != 0) ? ", " : "";
		resultString += Dice::diceSymbolToString(resultSymbols[i]);
	}
	resultString += "]";

	return resultString;
}

vector<int> DiceRoller::getPreviousRoll() const
{
	return previousRoll;
}

void DiceRoller::setPreviousRoll(vector<int> vec)
{
	previousRoll = vec;
}

void DiceRoller::clear()
{
	previousRoll = { 0, 0, 0, 0, 0, 0 };
}
