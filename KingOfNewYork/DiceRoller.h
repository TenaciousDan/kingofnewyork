#pragma once

#include <map>
#include <vector>
#include <string>

#include "Observable.h"
#include "GameObject.h"
#include "Dice.h"

using std::string;
using std::vector;

class DiceRoller : public GameObject, public Observable
{
private:
	int totalRolls;
	std::map<int, int> resultStats;
	vector<int> previousRoll;
	Dice die;

public:
	DiceRoller();
	~DiceRoller();

	void render() const override;

	vector<int> roll(int numberOfDice);
	static string resultsToString(vector<int> results);
	vector<int> getPreviousRoll() const;
	void setPreviousRoll(std::vector<int> vec);
	void clear();
};

