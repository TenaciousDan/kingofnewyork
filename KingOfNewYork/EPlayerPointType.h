#pragma once

enum class EPlayerPointType { HEALTH, ENERGY, VICTORY };