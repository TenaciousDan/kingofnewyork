#include "Game.h"

#include <iostream>

#include "exit_codes.h"
#include "MapLoader.h"
#include "io_utilities.h"
#include "common_utilities.h"
#include "CardLoader.h"

using std::cout;
using std::cin;

const string Game::CHARACTERS[] = { "CAPTAIN FISH", "MANTIS", "ROB", "KONG", "DRAKONIS", "SHERIFF" };

Game& Game::getInstance()
{
	static Game instance;
	return instance;
}

Game::Game() : game_board(MapLoader::DEFAULT_MAP_URL)
{
	running = false;
	current_player_index = -1;
}

Game::~Game()
{
	for (Player* player : players)
		delete player;
}

Game::EGameRulesPolicy Game::getGameRulesPolicy()
{
	return game_rules_policy;
}

vector<shared_ptr<Card>>& Game::getShop()
{
	return shop;
}

vector<shared_ptr<Card>>& Game::getDiscardPile()
{
	return discard_pile;
}

vector<shared_ptr<Card>>& Game::getGoalCards()
{
	return goal_cards;
}

vector<Player*>& Game::getPlayers()
{
	return players;
}

Deck<Card>& Game::getCardDeck()
{
	return card_deck;
}

GameBoard& Game::getGameBoard()
{
	return game_board;
}

Player& Game::getCurrentPlayer()
{
	return *(players[current_player_index]);
}

int Game::getAlivePlayerCount()
{
	int count = 0;
	for (Player* p : players)
		count += p->isDead() ? 0 : 1;

	return count;
}

void Game::printTitle()
{
	cout << "\n"
		R"(__/\\\________/\\\_______/\\\\\_______/\\\\\_____/\\\__/\\\________/\\\_)""\n"
		R"( _\/\\\_____/\\\//______/\\\///\\\____\/\\\\\\___\/\\\_\///\\\____/\\\/__)""\n"
		R"(  _\/\\\__ /\\\//______/\\\/__\///\\\__\/\\\/\\\__\/\\\___\///\\\/\\\/____)""\n"
		R"(   _\/\\\\\\//\\\______/\\\______\//\\\_\/\\\//\\\_\/\\\_____\///\\\/______)""\n"
		R"(    _\/\\\//_\//\\\____\/\\\_______\/\\\_\/\\\\//\\\\/\\\_______\/\\\_______)""\n"
		R"(     _\/\\\____\//\\\___\//\\\______/\\\__\/\\\_\//\\\/\\\_______\/\\\_______)""\n"
		R"(      _\/\\\_____\//\\\___\///\\\__/\\\____\/\\\__\//\\\\\\_______\/\\\_______)""\n"
		R"(       _\/\\\______\//\\\____\///\\\\\/_____\/\\\___\//\\\\\_______\/\\\_______)""\n"
		R"(        _\///________\///_______\/////_______\///_____\/////________\///________)""\n"
		<< '\n';
}

void Game::setup()
{
	Game::printTitle();
	cout << "This game is best played if you maximize the window.\n\n";

	selectBoard(common_utilities::dirList(MapLoader::MAP_DIRECTORY, ".map"));
	try
	{
		MapLoader loader;
		loader.load(game_board);
	}
	catch (const string& e)
	{
		cout << e << "\n";
		cout << "Press Enter to exit the program...";
		cin.get();
		exit(EXIT_MAP_LOAD_ERROR);
	}

	loadTileDecks();

	loadCards();

	setupPlayers();

	running = true;
}

void Game::selectBoard(vector<string> maps)
{
	if (maps.empty())
	{
		io_utilities::typewrite("Oops! You seem to be missing map data. Come back when you have a map to play on.");
		running = false;
		return;
	}

	int selection = -1;
	while (true)
	{
		io_utilities::typewrite("Please select a map to play on:\n");
		for (auto i = 0; i < maps.size(); i++)
			cout << i << ": " << maps[i] << '\n';

		string response;
		getline(cin, response);
		common_utilities::strTrim(response);

		bool isValid = true;
		if (!response.empty())
		{
			for (char c : response)
			{
				if (!common_utilities::isInt(c))
				{
					isValid = false;
					break;
				}
			}
			if (isValid && std::stoi(response) >= maps.size())
				isValid = false;
		}
		else
			isValid = false;

		if (isValid)
		{
			selection = std::stoi(response);
			break;
		}
		else
			io_utilities::typewrite("Selection invalid.\n");
	}

	game_board.setMapURL(MapLoader::MAP_DIRECTORY + maps[selection]);
}

void Game::setupPlayers()
{
	io_utilities::typewrite("\nWelcome to King Of " + game_board.getName() + "!\n");

	int numHumans = 0;
	int numBots = 0;
	while (true)
	{
		numHumans = 0;
		numBots = 0;
		while (true)
		{
			numHumans = io_utilities::promptInt("How many human players?");
			if (numHumans >= 0 && numHumans < 6)
				break;
			else
				io_utilities::typewrite("invalid selection: please enter a number between 0 and 6\n");
		}
		while (true && numHumans < 6)
		{
			numBots = io_utilities::promptInt("How many bots?");
			if (numBots >= 0 && numBots <= (6 - numHumans))
				break;
			else
				io_utilities::typewrite("invalid selection: please enter a number between 0 and " + std::to_string(6 - numHumans) + "\n");
		}

		if ((numHumans + numBots) <= 1)
			io_utilities::typewrite("There must be at least 2 players to play. Please try again...\n");
		else if ((numHumans + numBots) > 6)
			io_utilities::typewrite("The player limit is 6. Please try again...\n");
		else
			break;
	}

	int numPlayers = (numHumans + numBots);

	// set GameRules Policy
	if (numPlayers < 5)
		game_rules_policy = EGameRulesPolicy::FourPlayerPolicy;
	else
	{
		bool response = io_utilities::prompt("Would you like to use the advanced rules for 5-6 players?");
		game_rules_policy = response ? EGameRulesPolicy::SixPlayerPolicy : EGameRulesPolicy::FourPlayerPolicy;
	}

	current_player_index = 0;
	for (auto i = 0; i < numHumans; i++)
	{
		Player* player = new Player(string("P") + std::to_string(i + 1));
		player->registerObserver(&player_phase_observer);
		player->getDiceRoller()->registerObserver(&dice_observer);
		players.push_back(player);
	}

	int botstrat = 1;
	for (auto i = 0; i < numBots; i++)
	{
		Player* player = new Player(string("P") + std::to_string(numHumans + i + 1) + " BOT", static_cast<Player::EStrategy>(botstrat));
		player->registerObserver(&player_phase_observer);
		player->getDiceRoller()->registerObserver(&dice_observer);
		players.push_back(player);
		botstrat = botstrat == 1 ? 2 : 1;
	}

	// roll to see who will start
	cout << '\n';
	int rollVictors = 0;
	vector<int> rollResults(numPlayers);
	for (auto i = 0; i < numPlayers; i++)
	{
		if (rollResults[i] < 0)
			continue;

		string msg = players[i]->getName() + " dice result: ";
		io_utilities::typewrite(msg);

		rollResults[i] = 0;
		DiceRoller roller;
		vector<int> results = roller.roll(8);
		for (int r : results)
		{
			if (static_cast<Dice::EDiceSymbols>(r) == Dice::EDiceSymbols::ATTACK)
				rollResults[i]++;
		}
		cout << "<<" << rollResults[i] << " ATTACK>> " << DiceRoller::resultsToString(results) << '\n';

		if (i == (numPlayers - 1))
		{
			int highestRoller = 0;
			int highestRoll = 0;
			for (auto r = 0; r < rollResults.size(); r++)
			{
				if (rollResults[r] > highestRoll)
				{
					highestRoll = rollResults[r];
					rollVictors = 1;
					highestRoller = r;
				}
				else if (rollResults[r] == highestRoll)
					rollVictors++;
				else
					rollResults[r] = -1; // this roller is out
			}

			if (rollVictors > 1)
			{
				io_utilities::typewrite("There is a tie!!\n");
				i = -1; // reset loop and continue
				rollVictors = 0;
				for (auto r = 0; r < rollResults.size(); r++)
				{
					if (rollResults[r] != highestRoll)
						rollResults[r] = -1; // this roller is out
				}
			}
			else
				current_player_index = highestRoller;
		}
	}

	// select character names
	cout << '\n';
	vector<string> choices;
	for (string characterName : CHARACTERS)
		choices.push_back(characterName);

	for (auto i = 0; i < players.size(); i++)
	{
		int chosenIndex = players[current_player_index]->chooseCharacter(choices);
		choices.erase(choices.begin() + chosenIndex);
		current_player_index = (current_player_index + 1) % players.size();
	}

	// set main borough player limit
	if (game_rules_policy == Game::EGameRulesPolicy::FourPlayerPolicy)
		getGameBoard().getMainBorough()->setPlayerLimit(1);
	else
		getGameBoard().getMainBorough()->setPlayerLimit(2);

	// pick starting boroughs
	cout << '\n';

	vector<string> boroughLabels;
	for (Borough& b : getGameBoard().getBoroughs())
		boroughLabels.push_back(b.getName());

	string mainBoroughLabel = getGameBoard().getMainBorough()->getName();
	std::transform(mainBoroughLabel.begin(), mainBoroughLabel.end(), mainBoroughLabel.begin(), ::tolower);
	boroughLabels.erase(remove(boroughLabels.begin(), boroughLabels.end(), mainBoroughLabel), boroughLabels.end()); // remove main borough
	for (auto i = 0; i < players.size(); i++)
	{
		int chosenIndex = players[current_player_index]->chooseStartingBorough(boroughLabels);

		if (getGameBoard().getBoroughs()[chosenIndex + 1].isFull())
			boroughLabels.erase(boroughLabels.begin() + chosenIndex);

		current_player_index = (current_player_index + 1) % players.size();
	}
}

void Game::loadTileDecks()
{
	Deck<Tile> tileDeck;
	for (auto i = 0; i < 45; i++)
	{
		if (i < 22)
		{
			shared_ptr<Tile> ptrTile(new Tile(Tile::ETileType::BUILDING, Tile::ETileFace::OFFICE, 1));
			tileDeck.add(ptrTile);
		}
		else if (i < 25)
		{
			shared_ptr<Tile> ptrTile(new Tile(Tile::ETileType::BUILDING, Tile::ETileFace::POWER_PLANT, 2));
			tileDeck.add(ptrTile);
		}
		else if (i < 28)
		{
			shared_ptr<Tile> ptrTile(new Tile(Tile::ETileType::BUILDING, Tile::ETileFace::HOSPITAL, 2));
			tileDeck.add(ptrTile);
		}
		else if (i < 36)
		{
			shared_ptr<Tile> ptrTile(new Tile(Tile::ETileType::BUILDING, Tile::ETileFace::OFFICE, 2));
			tileDeck.add(ptrTile);
		}
		else if (i < 39)
		{
			shared_ptr<Tile> ptrTile(new Tile(Tile::ETileType::BUILDING, Tile::ETileFace::POWER_PLANT, 3));
			tileDeck.add(ptrTile);
		}
		else if (i < 42)
		{
			shared_ptr<Tile> ptrTile(new Tile(Tile::ETileType::BUILDING, Tile::ETileFace::HOSPITAL, 3));
			tileDeck.add(ptrTile);
		}
		else if (i < 45)
		{
			shared_ptr<Tile> ptrTile(new Tile(Tile::ETileType::BUILDING, Tile::ETileFace::OFFICE, 3));
			tileDeck.add(ptrTile);
		}
	}
	tileDeck.shuffle();
	
	for (auto i = 0; i < getGameBoard().getBoroughs().size(); i++)
	{
		if (tileDeck.size() > 0)
		{
			vector<Deck<Tile>> subTileDecks;
			Deck<Tile> subTileDeck1;
			subTileDeck1.add(tileDeck.draw());
			subTileDeck1.add(tileDeck.draw());
			subTileDeck1.add(tileDeck.draw());
			subTileDecks.push_back(subTileDeck1);
			Deck<Tile> subTileDeck2;
			subTileDeck2.add(tileDeck.draw());
			subTileDeck2.add(tileDeck.draw());
			subTileDeck2.add(tileDeck.draw());
			subTileDecks.push_back(subTileDeck2);
			Deck<Tile> subTileDeck3;
			subTileDeck3.add(tileDeck.draw());
			subTileDeck3.add(tileDeck.draw());
			subTileDeck3.add(tileDeck.draw());
			subTileDecks.push_back(subTileDeck3);

			getGameBoard().getBoroughs()[i].setTileDecks(subTileDecks);
		}
	}
}

void Game::loadCards()
{
	CardLoader cardLoader;
	cardLoader.load(card_deck, goal_cards, card_observer);

	card_deck.shuffle();
	shop.push_back(card_deck.draw());
	shop.push_back(card_deck.draw());
	shop.push_back(card_deck.draw());
}

void Game::kickPlayer(int index)
{
	players[index]->getBorough()->removePlayer(players[index]);

	delete players[index];
	players.erase(players.begin() + index);

	if (current_player_index >= players.size())
		current_player_index = 0;
}

void Game::loop()
{
	while (running)
	{
		update();
	}
}

bool Game::isRunning()
{
	return running;
}

void Game::update()
{
	if (getAlivePlayerCount() < 5 && game_rules_policy == Game::EGameRulesPolicy::SixPlayerPolicy)
	{ 
		// there is less than 5 players alive so advanced rules no longer apply
		game_rules_policy = Game::EGameRulesPolicy::FourPlayerPolicy;
		game_board.getMainBorough()->setPlayerLimit(1);
	}

	if (players.size() <= 0)
	{ 
		// all players have died, so nobody wins.
		io_utilities::typewrite("\nYou and all your rivals are dead. The city has vanquished all monsters.\nThis is a triumphet win for the human race!\n");
		running = false;
	}
	else
	{
		if (players[current_player_index]->isDead())
		{
			io_utilities::typewrite(players[current_player_index]->getName() + " has been eliminated!\n");
			kickPlayer(current_player_index);
		}
		else
		{
			if (this->players.size() == 1)
			{ 
				// one monster has survived and is victorious.
				io_utilities::typewrite("\nCongratulations " + players[current_player_index]->getName() + ", you are the winner. No one is left to stop you from your goal of world domination!\n");
				running = false;
			}
			else
			{
				players[current_player_index]->update();
				if (players[current_player_index]->isDead())
				{
					io_utilities::typewrite(players[current_player_index]->getName() + " has been eliminated!");
					kickPlayer(current_player_index);
				}
				else if (players[current_player_index]->getPoints(EPlayerPointType::VICTORY) >= 20)
				{ 
					// this monster has reached 20 VICTORY_POINT.
					io_utilities::typewrite("\nCongratulations " + players[current_player_index]->getName() + ", you are the winner. The other monsters yield to your supreme strength!\n");
					running = false;
				}
				else
					current_player_index = (current_player_index + 1) >= players.size() ? 0 : (current_player_index + 1);
			}
		}
	}
}

void Game::render()
{
	this->renderGameBoard();
	this->renderPlayers();
	this->renderShop();
}

void Game::renderGameBoard()
{
	cout << '\n';
	cout << "**************************************************" << '\n';
	cout << "* GAME BOARD                                     *" << '\n';
	cout << "**************************************************" << '\n';
	game_board.render();
	cout << "**************************************************" << '\n';
}

void Game::renderShop()
{
	cout << "**************************************************" << '\n';
	cout << "* CARD SHOP                                      *" << '\n';
	cout << "**************************************************" << '\n';
	for (auto i = 0; i < shop.size(); i++)
	{
		if (i != 0) cout << "==================================================" << '\n';
		shop[i]->render();
	}
	cout << "**************************************************" << '\n';
}

void Game::renderPlayers()
{
	cout << "**************************************************" << '\n';
	cout << "* MONSTERS                                       *" << '\n';
	cout << "**************************************************" << '\n';
	for (auto i = 0; i < Game::getInstance().getPlayers().size(); i++)
	{
		if (!players[i]->isDead())
		{
			if (i != 0) cout << "==================================================" << '\n';
			players[i]->render();
		}
	}
	cout << "**************************************************" << '\n';
}