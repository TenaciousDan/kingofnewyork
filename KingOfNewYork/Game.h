#pragma once

#include <string>
#include <vector>
#include <memory>

#include "Player.h"
#include "GameBoard.h"
#include "Deck.h"
#include "Card.h"
#include "CardObserver.h"
#include "PlayerPhaseObserver.h"
#include "DiceObserver.h"

using std::vector;
using std::string;
using std::shared_ptr;

class Game
{
public:
	enum class EGameRulesPolicy { FourPlayerPolicy, SixPlayerPolicy };

private:
	Game();
	bool running;
	Game::EGameRulesPolicy game_rules_policy;
	vector<Player*> players;
	int current_player_index;
	GameBoard game_board;
	Deck<Card> card_deck;
	vector<shared_ptr<Card>> goal_cards;
	vector<shared_ptr<Card>> shop;
	vector<shared_ptr<Card>> discard_pile;
	PlayerPhaseObserver player_phase_observer;
	CardObserver card_observer;
	DiceObserver dice_observer;

	static void printTitle();
	void selectBoard(vector<string> maps);
	void setupPlayers();
	void loadTileDecks();
	void loadCards();
	void kickPlayer(int index);

public:
	~Game();

	// singleton pattern
	static Game& getInstance();
	Game(Game const&) = delete;
	void operator=(Game const&) = delete;

	const static string CHARACTERS[];

	vector<shared_ptr<Card>>& getShop();
	vector<shared_ptr<Card>>& getDiscardPile();
	vector<shared_ptr<Card>>& getGoalCards();
	EGameRulesPolicy getGameRulesPolicy();
	vector<Player*>& getPlayers();
	int getAlivePlayerCount();
	Deck<Card>& getCardDeck();
	GameBoard& getGameBoard();
	Player& getCurrentPlayer();

	bool isRunning();

	void setup();
	void loop();
	void update();
	void render();
	void renderGameBoard();
	void renderShop();
	void renderPlayers();
};

