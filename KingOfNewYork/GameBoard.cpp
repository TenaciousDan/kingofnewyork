#include "GameBoard.h"

#include <iostream>

using std::cout;

GameBoard::GameBoard() : GameBoard("") {}
GameBoard::GameBoard(string mapURL) : boroughs(0)
{
	this->mapURL = mapURL;
	name = "";
	author = "";
}

GameBoard::~GameBoard()
{
}

string GameBoard::getMapUrl()
{
	return mapURL;
}

void GameBoard::setMapURL(string mapURL)
{
	this->mapURL = mapURL;
}

string GameBoard::getName()
{
	return this->name;
}

void GameBoard::setName(string name)
{
	this->name = name;
}

string GameBoard::getAuthor()
{
	return author;
}

void GameBoard::setAuthor(string author)
{
	this->author = author;
}

vector<Borough>& GameBoard::getBoroughs()
{
	return boroughs;
}

Borough* GameBoard::getMainBorough()
{
	return &boroughs[0];
}

void GameBoard::render() const
{
	for (auto i = 0; i < boroughs.size(); i++)
	{
		if (i != 0) cout << "==================================================" << '\n';
		boroughs[i].render();
	}
}