#pragma once

#include <string>
#include <vector>

#include "GameObject.h"
#include "Borough.h"

using std::string;
using std::vector;

class GameBoard : public GameObject
{
private:
	string mapURL;
	string name;
	string author;
	vector<Borough> boroughs;

public:
	GameBoard();
	GameBoard(string mapURL);
	~GameBoard();

	std::string getMapUrl();
	void setMapURL(string mapURL);
	std::string getName();
	void setName(string name);
	std::string getAuthor();
	void setAuthor(string author);
	vector<Borough>& getBoroughs();
	Borough* getMainBorough();
	void render() const override;
};

