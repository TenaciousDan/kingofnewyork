#include "HumanPlayerStrategy.h"

#include <iostream>

#include "io_utilities.h"
#include "common_utilities.h"
#include "Game.h"

HumanPlayerStrategy::HumanPlayerStrategy(Player* player) : PlayerStrategy(player)
{
}

HumanPlayerStrategy::~HumanPlayerStrategy()
{
}

int HumanPlayerStrategy::chooseCharacter(std::vector<std::string> characters)
{
	int selection = -1;
	while (true)
	{
		std::vector<int> chosen = io_utilities::promptChoice(player->getName() + " please select a character", characters);
		int chosenIndex = (!chosen.empty() && chosen[0] > 0 && chosen[0] <= characters.size()) ? chosen[0] - 1 : -1;

		if (chosenIndex == -1)
		{
			io_utilities::typewrite("Selection invalid.\n");
			continue;
		}
		else
		{
			selection = chosenIndex;
			std::cout << player->getName() << " => ";
			player->setName(characters[chosenIndex]);
			std::cout << player->getName() << std::endl;
			break;
		}
	}

	return selection;
}

int HumanPlayerStrategy::chooseStartingBorough(std::vector<std::string> boroughLabels)
{
	int selection = -1;
	while (true)
	{
		std::vector<int> chosen = io_utilities::promptChoice(player->getName() + " please select a starting borough", boroughLabels);
		int chosenIndex = (!chosen.empty() && chosen[0] > 0 && chosen[0] <= boroughLabels.size()) ? chosen[0] - 1 : -1;

		if (chosenIndex == -1)
		{
			io_utilities::typewrite("Selection invalid.\n");
			continue;
		}
		else
		{
			selection = chosenIndex;
			std::cout << player->getName() << " => ";
			Game::getInstance().getGameBoard().getBoroughs()[chosenIndex + 1].addPlayer(player);
			player->setBorough(&Game::getInstance().getGameBoard().getBoroughs()[chosenIndex+1]);
			std::cout << player->getBorough()->getName() << std::endl;
			break;
		}
	}

	return selection;
}

void HumanPlayerStrategy::rollDice(std::vector<int>& results)
{
	int numDice = results.size();

	bool again = true;
	int numberOfRolls = 1;
	while (again && numberOfRolls < player->getMaxRolls())
	{
		again = io_utilities::prompt(player->getName() + ", would you like to roll again?");
		if (again)
		{
			std::vector<std::string> choices;
			for (int i = 0; i < results.size(); i++)
				choices.push_back(Dice::diceSymbolToString(static_cast<Dice::EDiceSymbols>(results[i])));

			std::vector<int> chosen = io_utilities::promptChoice("Chose dice to reroll", choices, false);
			for (int i : chosen)
			{
				if (i > 0 && i <= results.size())
					results[(i - 1)] = -1; // mark chosen index for removal
			}
			results.erase(remove(results.begin(), results.end(), -1), results.end()); // remove all marked index
			std::cout << "Locked dice: " << player->getDiceRoller()->resultsToString(results) << std::endl;
			std::vector<int> results2 = player->getDiceRoller()->roll(numDice - results.size());
			results.insert(results.end(), results2.begin(), results2.end());
			player->getDiceRoller()->setPreviousRoll(results);
		}

		numberOfRolls++;
	}
}

void HumanPlayerStrategy::resolveDice(std::map<Dice::EDiceSymbols, int> diceResults)
{
	std::vector<std::string> choices;
	for (auto result : diceResults)
		choices.push_back(Dice::diceSymbolToString(result.first));

	while (choices.size() > 0 && !player->isDead())
	{
		std::vector<int> chosen = io_utilities::promptChoice(player->getName() + ", chose resolution order of dice", choices, false);
		for (int choice : chosen)
		{
			if (choice > 0 && choice <= choices.size())
			{
				player->resolve(Dice::strToDiceSymbol(choices[choice - 1]), diceResults[Dice::strToDiceSymbol(choices[choice - 1])]);
				choices[choice - 1] = ""; // mark for removal
			}
		}
		choices.erase(remove(choices.begin(), choices.end(), ""), choices.end()); // remove marked

		if (choices.size() == 1)
		{
			player->resolve(Dice::strToDiceSymbol(choices[0]), diceResults[Dice::strToDiceSymbol(choices[0])]);
			choices.clear();
		}

		if (choices.size() > 1 && !player->isDead())
			io_utilities::typewrite("There are still some dice to resolve. ");
	}
}

bool HumanPlayerStrategy::flee()
{
	return io_utilities::prompt(player->getName() + ", would you like to flee to another borough?");
}

int HumanPlayerStrategy::destroy(std::vector<Deck<Tile>*> buildingTargets, std::vector<std::shared_ptr<Tile>> militaryTargets)
{
	int selection = -1;

	std::string response;
	std::cin.clear();
	std::getline(std::cin, response);
	common_utilities::strTrim(response);
	bool isValid = true;
	if (!response.empty())
	{
		for (char c : response)
		{
			if (!common_utilities::isInt(c))
			{
				isValid = false;
				break;
			}
		}
		if (isValid && std::stoi(response) >= (buildingTargets.size() + militaryTargets.size()))
			isValid = false;
	}
	else
		isValid = false;

	if (isValid)
		selection = std::stoi(response);
	else
		io_utilities::typewrite("Selection invalid.\n");

	return selection;
}

void HumanPlayerStrategy::move(std::vector<Borough*> movementOptions, bool mandatoryMovement)
{
	std::vector<std::string> choices;
	for (Borough* option : movementOptions)
		choices.push_back(option->getName());

	int selection = -1;
	while (true)
	{
		std::vector<int> chosen = io_utilities::promptChoice(player->getName() + " where would you like to move?", choices, !mandatoryMovement);
		for (int i : chosen)
		{
			if (i >= 0 && i <= choices.size())
			{
				if (i == 0 && mandatoryMovement) continue;

				selection = i;
				break;
			}
		}

		if (selection > 0 && selection <= movementOptions.size())
		{
			player->moveToBorough(movementOptions[selection - 1]);
			break;
		}
		else if (selection == 0 && !mandatoryMovement)
			break;
		else if (selection == -1)
			io_utilities::typewrite("Selection invalid.\n");
	}
}

void HumanPlayerStrategy::buyCards(std::vector<std::shared_ptr<Card>>& shop)
{
	std::vector<std::string> choices;
	choices.push_back("REFRESH SHOP");
	for (int i = 0; i < shop.size(); i++)
		choices.push_back(shop[i]->getName());

	bool loop = true;
	while (loop)
	{
		// check if the player can do anything
		if (shop.empty())
		{
			io_utilities::typewrite("There is nothing left in the shop to buy.");
			break;
		}

		std::vector<int> chosen = io_utilities::promptChoice(player->getName() + ", you have " + std::to_string(player->getPoints(EPlayerPointType::ENERGY)) + " ENERGY, select a card from the shop to buy", choices, true);
		for (int i : chosen)
		{
			if (i > 0 && i < shop.size() + 2)
			{
				if (i == 1)
				{ 
					// selected to refresh shop
					if (player->getPoints(EPlayerPointType::ENERGY) >= 2)
					{
						player->addPoints(EPlayerPointType::ENERGY, -2);

						for (auto s = 0; s < shop.size(); s++)
							Game::getInstance().getDiscardPile().push_back(shop[s]);
						shop.clear();
						choices.erase(choices.begin() + 1, choices.end());

						for (auto s = 0; s < 3; s++)
						{
							std::shared_ptr<Card> drawnCard = Game::getInstance().getCardDeck().draw();
							if (drawnCard != nullptr)
							{
								shop.push_back(drawnCard);
								choices.push_back(drawnCard->getName());
							}
							else break;
						}

						if (!shop.empty())
						{
							Game::getInstance().renderShop();
						}

						break;
					}
					else
					{
						io_utilities::typewrite(player->getName() + ", you do not have enough ENERGY to perform this action.\n");
						break;
					}
				}
				else if (player->getPoints(EPlayerPointType::ENERGY) >= shop[i - 2]->getCost())
				{ 
					// selected a card
					player->addPoints(EPlayerPointType::ENERGY, -shop[i - 2]->getCost());

					if (!(shop[i - 2]->getType() == Card::CARD_TYPE::DISCARD))
						player->getHand().push_back(shop[i - 2]);
					else
					{
						player->getHand().push_back(shop[i - 2]);
						Game::getInstance().getDiscardPile().push_back(player->getHand()[player->getHand().size() - 1]);
						player->getHand().pop_back();
					}

					shop.erase(shop.begin() + (i - 2));
					choices.erase(choices.begin() + (i - 2));

					std::shared_ptr<Card> drawnCard = Game::getInstance().getCardDeck().draw();
					if (drawnCard != nullptr)
					{
						shop.push_back(drawnCard);
						choices.push_back(drawnCard->getName());
					}

					Game::getInstance().renderShop();
					break;
				}
				else
				{ 
					// selected a card but cannot afford it
					io_utilities::typewrite(player->getName() + ", you cannot afford '" + shop[i - 2]->getName() + "'\n");
					break;
				}
			}
			else if (i == 0)
			{
				loop = false;
				break;
			}
		}
	}
}