#include "MapLoader.h"

#include <string>
#include <vector>
#include <fstream>
#include <regex>

#include "common_utilities.h"
#include "Game.h"
#include "Borough.h"

using std::vector;

const std::string MapLoader::MAP_DIRECTORY = "data/maps/";
const std::string MapLoader::DEFAULT_MAP_URL = MAP_DIRECTORY + "New York.map";

MapLoader::MapLoader()
{
}

MapLoader::~MapLoader()
{
}

// throws std::string if there is a problem reading map file
void MapLoader::load(GameBoard& gameBoard)
{
	std::ifstream inFile(gameBoard.getMapUrl());

	if (!inFile)
		throw string("Cannot open or read file.");

	// debug("Loading map file...");
	string line;
	string section = "";
	int section_count = 0;
	while (getline(inFile, line))
	{
		common_utilities::strTrim(line);
		// debug(line);
		if (line == "") continue;

		if (std::regex_match(line, std::regex("^\\[.+\\]$")))
		{
			std::transform(line.begin(), line.end(), line.begin(), ::tolower);
			section = (line == "[end]") ? "" : line;
			section_count = 0;
			continue;
		}

		if (section == "[map]")
		{
			if (section_count == 0)
				gameBoard.setName(line);
			else if (section_count == 1)
				gameBoard.setAuthor(line);

			section_count++;
		}
		else if (section == "[regions]")
		{
			std::transform(line.begin(), line.end(), line.begin(), ::tolower);
			Borough borough(line);
			gameBoard.getBoroughs().push_back(borough);

			section_count++;
		}
	}

	if (gameBoard.getBoroughs().size() < 4)
		throw string("invalid map. not enough nodes.");

	// debug("Finished loading map file...");

	inFile.close();
}