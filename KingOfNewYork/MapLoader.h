#pragma once

#include <vector>

#include "GameBoard.h"

class MapLoader
{
public:
	MapLoader();
	~MapLoader();

	const static string MAP_DIRECTORY;
	const static string DEFAULT_MAP_URL;

	void load(GameBoard& gameBoard);
};

