#include "ModerateAIPlayerStrategy.h"

#include <iostream>

#include "io_utilities.h"
#include "Game.h"

ModerateAIPlayerStrategy::ModerateAIPlayerStrategy(Player* player) : AIPlayerStrategy(player)
{
}

ModerateAIPlayerStrategy::~ModerateAIPlayerStrategy()
{
}

void ModerateAIPlayerStrategy::rollDice(std::vector<int>& results)
{
	int numDice = results.size();

	bool again = true;
	int numberOfRolls = 1;
	while (again)
	{
		if (numberOfRolls >= player->getMaxRolls()) break;

		std::vector<int> chosen;
		for (auto i = 0; i < results.size(); i++)
		{
			if (
				results[i] != static_cast<int>(Dice::EDiceSymbols::VICTORY) && results[i] != static_cast<int>(Dice::EDiceSymbols::ENERGY) && results[i] != static_cast<int>(Dice::EDiceSymbols::HEART) &&
				(player->getBorough() != Game::getInstance().getGameBoard().getMainBorough() && Game::getInstance().getGameBoard().getMainBorough()->isFull() && results[i] != static_cast<int>(Dice::EDiceSymbols::ATTACK))
				)
				chosen.push_back(i);
		}

		again = !chosen.empty();
		if (again)
		{
			io_utilities::typewrite(player->getName() + ", chooses to reroll some dice. \n");

			for (int i : chosen)
			{
				if (i >= 0 && i < results.size())
					results[i] = -1; // mark chosen index for removal
			}
			results.erase(remove(results.begin(), results.end(), -1), results.end()); // remove all marked index
			std::cout << "Locked dice: " << player->getDiceRoller()->resultsToString(results) << std::endl;
			std::vector<int> results2 = player->getDiceRoller()->roll(numDice - results.size());
			results.insert(results.end(), results2.begin(), results2.end());
			player->getDiceRoller()->setPreviousRoll(results);
		}

		numberOfRolls++;
	}
}

void ModerateAIPlayerStrategy::resolveDice(std::map<Dice::EDiceSymbols, int> diceResults)
{
	if (!player->isDead())
	{
		if (!diceResults.empty())
		{
			if (diceResults.find(Dice::EDiceSymbols::VICTORY) != diceResults.end())
			{
				player->resolve(Dice::EDiceSymbols::VICTORY, diceResults[Dice::EDiceSymbols::VICTORY]);
				diceResults[Dice::EDiceSymbols::VICTORY] = -1; // mark as processed
			}
			if (diceResults.find(Dice::EDiceSymbols::HEART) != diceResults.end())
			{
				player->resolve(Dice::EDiceSymbols::HEART, diceResults[Dice::EDiceSymbols::HEART]);
				diceResults[Dice::EDiceSymbols::HEART] = -1; // mark as processed
			}
			if (diceResults.find(Dice::EDiceSymbols::ENERGY) != diceResults.end())
			{
				player->resolve(Dice::EDiceSymbols::ENERGY, diceResults[Dice::EDiceSymbols::ENERGY]);
				diceResults[Dice::EDiceSymbols::ENERGY] = -1; // mark as processed
			}

			for (auto it = diceResults.begin(); it != diceResults.end(); it++)
			{
				if (it->second != -1)
					player->resolve(it->first, it->second);
			}
		}
	}
}

bool ModerateAIPlayerStrategy::flee()
{
	return (player->getPoints(EPlayerPointType::HEALTH) <= player->getMaxHealth() / 3);
}

int ModerateAIPlayerStrategy::destroy(std::vector<Deck<Tile>*> buildingTargets, std::vector<std::shared_ptr<Tile>> militaryTargets)
{
	return AIPlayerStrategy::destroy(buildingTargets, militaryTargets);
}

void ModerateAIPlayerStrategy::move(std::vector<Borough*> movementOptions, bool mandatoryMovement)
{
	AIPlayerStrategy::move(movementOptions, mandatoryMovement);
}

void ModerateAIPlayerStrategy::buyCards(std::vector<std::shared_ptr<Card>>& shop)
{
	AIPlayerStrategy::buyCards(shop);
}