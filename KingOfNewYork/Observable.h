#pragma once

#include <list>

#include "Observer.h"

class Observer;

class Observable
{
private:
	std::list<Observer*> observers;

public:
	virtual void registerObserver(Observer* observer);
	virtual void unregisterObserver(Observer* observer);
	virtual void notify();
};

