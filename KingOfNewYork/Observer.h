#pragma once

#include "Observable.h"

class Observable;

class Observer
{
public:
	virtual void onNotify(const Observable* observable) = 0;
};

