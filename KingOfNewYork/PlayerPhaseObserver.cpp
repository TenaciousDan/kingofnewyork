#include "PlayerPhaseObserver.h"

#include <iostream>

#include "Player.h"
#include "Game.h"

using std::cout;

void PlayerPhaseObserver::onNotify(const Observable* observable)
{
	if (const Player* player = dynamic_cast<const Player*>(observable))
	{
		if (player->getPhase() == Player::EPlayerPhase::STANDBY)
		{
			if (player->getSubPhase() == Player::EPlayerSubPhase::START)
			{
				Game::getInstance().render();
				cout << '\n' << "----- " << player->getName() << " - STANDBY PHASE -----" << '\n';
			}
			else if (player->getSubPhase() == Player::EPlayerSubPhase::END)
			{
				if (player->getBorough() == Game::getInstance().getGameBoard().getMainBorough())
					player->renderPoints();
			}
		}
		else if (player->getPhase() == Player::EPlayerPhase::ROLL_DICE)
		{
			if (player->getSubPhase() == Player::EPlayerSubPhase::START)
			{
				cout << '\n' << "----- " << player->getName() << " - ROLL DICE PHASE -----" << '\n';
				player->getDiceRoller()->render();
			}
			else if (player->getSubPhase() == Player::EPlayerSubPhase::END)
			{
				//
			}
		}
		else if (player->getPhase() == Player::EPlayerPhase::RESOLVE_DICE)
		{
			if (player->getSubPhase() == Player::EPlayerSubPhase::START)
			{
				cout << '\n' << "----- " << player->getName() << " - DICE RESOLUTION PHASE -----" << '\n';
			}
			else if (player->getSubPhase() == Player::EPlayerSubPhase::END)
			{
				//
			}
		}
		else if (player->getPhase() == Player::EPlayerPhase::MOVE)
		{
			if (player->getSubPhase() == Player::EPlayerSubPhase::START)
			{
				cout << '\n' << "----- " << player->getName() << " - MOVEMENT PHASE -----" << '\n';
			}
			else if (player->getSubPhase() == Player::EPlayerSubPhase::END)
			{
				Game::getInstance().renderGameBoard();
			}
		}
		else if (player->getPhase() == Player::EPlayerPhase::BUY_CARDS)
		{
			if (player->getSubPhase() == Player::EPlayerSubPhase::START)
			{
				cout << '\n' << "----- " << player->getName() << " - BUY CARDS PHASE -----" << '\n';
				Game::getInstance().renderShop();
			}
			else if (player->getSubPhase() == Player::EPlayerSubPhase::END)
			{
				//
			}
		}
		else if (player->getPhase() == Player::EPlayerPhase::END)
		{
			if (player->getSubPhase() == Player::EPlayerSubPhase::START)
			{
				cout << '\n' << "----- " << player->getName() << " - END PHASE -----" << '\n';
			}
			else if (player->getSubPhase() == Player::EPlayerSubPhase::END)
			{
				//
			}
		}
	}
}