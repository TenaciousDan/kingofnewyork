#pragma once

#include "Observer.h"

#include <iostream>

#include "Player.h"

class PlayerPhaseObserver : public Observer
{
public:
	void onNotify(const Observable* observable);
};

