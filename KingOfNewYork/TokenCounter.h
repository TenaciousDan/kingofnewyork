#pragma once

#include <string>
#include <vector>

#include "GameObject.h"

using std::vector;
using std::string;

class TokenCounter : public GameObject
{
public:
	enum class ETokenType {
		WEB, JINX, SOUVENIR, CARAPACE,
		SIZE
	};

private:
	vector<int> counters;

public:
	TokenCounter();
	~TokenCounter();

	int getTokenCount() const;
	int getTokenCount(ETokenType token_type) const;
	void removeToken(ETokenType token_type);
	void addToken(ETokenType token_type);

	void render() const override;

	string toString();
};

