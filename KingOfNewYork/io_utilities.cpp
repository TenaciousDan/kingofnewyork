#include "io_utilities.h"

#include <iostream>
#include <chrono>
#include <thread>
#include <cctype>
#include <set>
#include <regex>

#include "common_utilities.h"

using std::cout;
using std::cin;

namespace io_utilities
{
	void typewrite(const string& str, int typing_delay)
	{
		for (int i = 0; str[i] != '\0'; i++)
		{
			cout << str[i] << std::flush;
			std::this_thread::sleep_for(std::chrono::milliseconds(typing_delay));
		}
	}

	bool prompt(const string& msg)
	{
		typewrite(msg + " (y/n): ");

		string response;
		getline(cin, response);
		common_utilities::strTrim(response);

		if (!response.empty() && std::tolower(response[0]) == 'y')
			return true;
		else
			return false;
	}

	vector<int> promptChoice(const string& msg, vector<string> strChoices, bool cancelOption)
	{
		typewrite(msg + " : ");

		cout << std::endl << "[" << (cancelOption ? "0: CANCEL" : "");
		for (auto i = 0; i < strChoices.size(); i++)
		{
			cout << ((i != 0 || cancelOption) ? ", " : "");
			cout << std::to_string(i + 1) + ": " + strChoices[i];
		}
		cout << "]" << std::endl;

		string response;
		getline(cin, response);
		common_utilities::strTrim(response);

		vector<string> strTokens;
		common_utilities::strSplit(response, strTokens, ' ');

		vector<int> choices;
		for (string strToken : strTokens)
		{
			if (!strToken.empty() && common_utilities::isInt(strToken[0]))
				choices.push_back(strToken[0] - '0');
		}

		auto end_unique = std::end(choices);
		for (auto iter = std::begin(choices); iter != end_unique; ++iter)
			end_unique = std::remove(iter + 1, end_unique, *iter);
		choices.erase(end_unique, std::end(choices));

		return choices;
	}

	int promptInt(const string& msg)
	{
		typewrite(msg + " : ");

		string response;
		getline(cin, response);
		common_utilities::strTrim(response);

		vector<string> strTokens;
		common_utilities::strSplit(response, strTokens, ' ');

		if (!strTokens.empty() && std::regex_match(strTokens[0], std::regex("^[-+]?\\d+$")))
			return std::stoi(strTokens[0]);
		else
			return -1;
	}
}